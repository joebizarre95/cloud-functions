module.exports = {
  root: false,
  env: {
    es6: true,
    node: true,
    es2022: true,
  },
  extends: [
    "eslint:recommended",
    "google",
  ],
  rules: {
    quotes: ["error", "double"],
  },
};
