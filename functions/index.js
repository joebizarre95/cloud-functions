const functions = require("firebase-functions");
const express = require("express");
const cors = require("cors");

const app = express();
app.use(cors());
app.use(require("./routes/user.routes"));

exports.utopia = functions.https.onRequest(app);
