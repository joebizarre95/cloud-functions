const {Router} = require("express");
// eslint-disable-next-line new-cap
const router = Router();
const admin = require("firebase-admin");
const mail = require("../emails/sender");

admin.initializeApp({
  credential: admin.credential.cert("./credentials.json"),
});
const db = admin.firestore();

// get payment
// eslint-disable-next-line require-jsdoc
async function getPackague(paymentId) {
  return await db.collection("utopia_packagues").doc(paymentId).get();
}

// create user
router.post("/user/create", async (req, res) => {
  await db.collection("utopia_users")
      .where("user_email", "==", req.body.user_email)
      .get()
      .then((response) => {
        if (response.docs[0] != undefined) {
          return res.status(500).json({
            message: "This user is registered in the platform ",
          });
        } else {
          createUser(req.body);
          const html = "<br>New applicant on the web:" +
          "<br>Full name:" + req.body.full_name +
          "<br>Email:" + req.body.user_email;
          mail.sendMail(html);
          return res.status(200).json({
            message: "success",
          });
          // eslint-disable-next-line no-unreachable
        }
      });
});

// eslint-disable-next-line require-jsdoc
async function createUser(body) {
  await db.collection("utopia_users").doc().create({
    full_name: body.full_name,
    user_email: body.user_email,
    accredited: false,
  });
  return true;
}

// create payments user list
router.post("/payment/create", async (req, res) => {
  const package = await getPackague(req.body.package_id);
  try {
    await db.collection("utopia_payments").doc().create({
      nft_numbers: req.body.nft_numbers,
      total_price: req.body.total_price,
      payment_method: req.body.payment_method,
      have_wallet: req.body.have_wallet,
      wallet: req.body.wallet,
      user_email: req.body.user_email,
      package_id: req.body.package_id,
      package: package.data(),
    });
    const html = `
      A new purchase form was filled out on the web with the following data:
      <br> Number NFTs: ${req.body.nft_numbers}
      <br> Total price: ${req.body.total_price}
      <br> Payment method: ${req.body.payment_method}
      <br> Have a wallet: ${req.body.have_wallet ? "SI" : "NO"}
      <br> Email: ${req.body.user_email}
      <br> Wallet address: ${req.body.wallet != "" ? req.body.wallet : "N/A"}
    `;
    mail.sendMail(html);
    return res.status(200).json({
      message: "success",
    });
  } catch (error) {
    return res.status(500).send(error);
  }
});

// get all users list
router.get("/users", async (req, res) => {
  const query = await db.collection("utopia_users");
  await query.get().then((response) => {
    const data = response.docs.map((item) => ({
      "full_name": item.data().full_name,
      "user_email": item.data().user_email,
      "accredited": item.data().accredited,
      "token": item.data().token,
    }));
    return res.status(200).json(data);
  }).catch((err) => {
    return res.status(500).send(err);
  });
});

// get filter user list
router.get("/users/:email", async (req, res) => {
  await db.collection("utopia_users")
      .orderBy("user_email")
      .startAt(req.params.email)
      .get()
      .then((response) => {
        const data = response.docs.map((item) => ({
          "first_name": item.data().first_name,
          "last_name": item.data().last_name,
          "user_email": item.data().user_email,
          "accredited": item.data().accredited,
          "token": item.data().token,
        }));
        return res.status(200).json(data);
      }).catch((err) => {
        return res.status(500).send(err);
      });
});

// get all payment list
router.get("/payments", async (req, res) => {
  const query = await db.collection("utopia_payments");
  await query.get().then((response) => {
    const data = response.docs.map((item) => ({
      nft_numbers: item.data().nft_numbers,
      total_price: item.data().total_price,
      payment_method: item.data().payment_method,
      have_wallet: item.data().have_wallet,
      wallet: item.data().wallet,
      user_email: item.data().user_email,
      package: item.data().package,
    }));
    return res.status(200).json(data.reverse());
  }).catch((err) => {
    return res.status(500).send(err);
  });
});

// get all payments with filter query
router.get("/payment/:email", async (req, res) => {
  await db.collection("utopia_payments")
      .orderBy("user_email")
      .startAt(req.params.email)
      .get()
      .then((response) => {
        const data = response.docs.map((item) => ({
          nft_numbers: item.data().nft_numbers,
          total_price: item.data().total_price,
          payment_method: item.data().payment_method,
          have_wallet: item.data().have_wallet,
          wallet: item.data().wallet,
          user_email: item.data().user_email,
        }));
        return res.status(200).json(data);
      }).catch((err) => {
        return res.status(500).send(err);
      });
});

// update specific user
router.put("/user/:token", async (req, res) => {
  try {
    const document = await db.collection("utopia_users")
        .where("token", "==", req.params.token)
        .get()
        .then((snapshot) => {
          return snapshot.docs[0];
        })
        .catch((err) => {
          return res.status(500).send(err);
        });
    await document.ref.update({
      accredited: req.body.accredited,
    });
    return res.status(200).json();
  } catch (error) {
    return res.status(500).send(error);
  }
});

router.get("/packages", async (req, res) => {
  const query = await db.collection("utopia_packagues");
  await query.get().then((response) => {
    const data = response.docs.map((item) => ({
      id: item.id,
      active: item.data().active,
      available: item.data().available,
      background: item.data().background,
      color: item.data().color,
      finishRange: item.data().finishRange,
      initRange: item.data().initRange,
      name: item.data().name,
      quantity: item.data().quantity,
      sold_out: item.data().sold_out,
      order: item.data().order,
    }));
    data.sort((a, b) => {
      return parseInt(a.order) - parseInt(b.order);
    });
    return res.status(200).json(data);
  });
});

module.exports = router;
